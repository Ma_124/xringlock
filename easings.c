/*
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <easing.h>

#include "easings.h"
#include "util.h"

#include "easings_gperf.c"
#include "logging.h"

#define STATIC_ASSERT(test) typedef char __assertion[( !!(test) )*2-1 ]

static const easing_t default_easing = {"sine_ease_in", 1};

const easing_t *easings_lookup(const char *str) {
    const easing_t* e = easings_lookup_str(str, strlen(str));
    if (e == NULL) {
        e = &default_easing;
        Log("unknown easing %s using default %s", str, e->name);
    }
    return e;
}

double ease(const easing_t *e, double x) {
    static AHEasingFunction easing_fns[] = {
            LinearInterpolation,
            QuadraticEaseIn,
            QuadraticEaseOut,
            QuadraticEaseInOut,
            CubicEaseIn,
            CubicEaseOut,
            CubicEaseInOut,
            QuarticEaseIn,
            QuarticEaseOut,
            QuarticEaseInOut,
            QuinticEaseIn,
            QuinticEaseOut,
            QuinticEaseInOut,
            SineEaseIn,
            SineEaseOut,
            SineEaseInOut,
            CircularEaseIn,
            CircularEaseOut,
            CircularEaseInOut,
            ExponentialEaseIn,
            ExponentialEaseOut,
            ExponentialEaseInOut,
            ElasticEaseIn,
            ElasticEaseOut,
            ElasticEaseInOut,
            BackEaseIn,
            BackEaseOut,
            BackEaseInOut,
            BounceEaseIn,
            BounceEaseOut,
            BounceEaseInOut,
    };

    STATIC_ASSERT(sizeof(easing_fns) == (EASINGS_TOTAL_KEYWORDS) * sizeof(AHEasingFunction));

    if (e->idx >= ARRAY_LEN(easing_fns)) {
        Log("ease: index %d outside of function table, using default %s", e->idx, default_easing.name);
        return easing_fns[0](x);
    }

    return easing_fns[e->idx](x);
}
