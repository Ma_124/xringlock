#pragma once

// This header is generated from config.in by generate_config.py

/*
 * Copyright 2014 Google Inc. All rights reserved.
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

static double cached_ring_radius = NAN;

//! RING_RADIUS specifies the radius of the ring.
#define CONFIG_RING_RADIUS (isnan(cached_ring_radius) ? cached_ring_radius = config_get_double("XRINGLOCK_RING_RADIUS", 100.0) : cached_ring_radius)

static color_t *cached_ring_fill_color = NULL;

//! RING_FILL_COLOR specifies the fill color of the ring.
#define CONFIG_RING_FILL_COLOR (cached_ring_fill_color == NULL ? cached_ring_fill_color = config_get_color("XRINGLOCK_RING_FILL_COLOR", (0x33/255.0), (0x33/255.0), (0x33/255.0), (0xcc/255.0)) : cached_ring_fill_color)

static color_t *cached_ring_fill_color_key = NULL;

//! RING_FILL_COLOR_KEY specifies the fill color of the ring after a keypress.
#define CONFIG_RING_FILL_COLOR_KEY (cached_ring_fill_color_key == NULL ? cached_ring_fill_color_key = config_get_color("XRINGLOCK_RING_FILL_COLOR_KEY", 0, 0, 0, 0) : cached_ring_fill_color_key)

static color_t *cached_ring_fill_color_back = NULL;

//! RING_FILL_COLOR_BACK specifies the fill color of the ring after a backspace keypress.
#define CONFIG_RING_FILL_COLOR_BACK (cached_ring_fill_color_back == NULL ? cached_ring_fill_color_back = config_get_color("XRINGLOCK_RING_FILL_COLOR_BACK", 0, 0, 0, 0) : cached_ring_fill_color_back)

static color_t *cached_ring_fill_color_clear = NULL;

//! RING_FILL_COLOR_CLEAR specifies the fill color of the ring after a clear keypress.
#define CONFIG_RING_FILL_COLOR_CLEAR (cached_ring_fill_color_clear == NULL ? cached_ring_fill_color_clear = config_get_color("XRINGLOCK_RING_FILL_COLOR_CLEAR", 0, 0, 0, 0) : cached_ring_fill_color_clear)

static color_t *cached_ring_fill_color_auth = NULL;

//! RING_FILL_COLOR_AUTH specifies the fill color of the ring during authentication.
#define CONFIG_RING_FILL_COLOR_AUTH (cached_ring_fill_color_auth == NULL ? cached_ring_fill_color_auth = config_get_color("XRINGLOCK_RING_FILL_COLOR_AUTH", 0, 0, 0, 0) : cached_ring_fill_color_auth)

static color_t *cached_ring_fill_color_caps = NULL;

//! RING_FILL_COLOR_CAPS specifies the fill color of the ring if caps lock is enabled.
#define CONFIG_RING_FILL_COLOR_CAPS (cached_ring_fill_color_caps == NULL ? cached_ring_fill_color_caps = config_get_color("XRINGLOCK_RING_FILL_COLOR_CAPS", (0xf4/255.0), (0x43/255.0), (0x36/255.0), 1.0) : cached_ring_fill_color_caps)

static color_t *cached_ring_border_color = NULL;

//! RING_BORDER_COLOR specifies the border color of the ring.
#define CONFIG_RING_BORDER_COLOR (cached_ring_border_color == NULL ? cached_ring_border_color = config_get_color("XRINGLOCK_RING_BORDER_COLOR", 0, 0, 0, 0) : cached_ring_border_color)

static color_t *cached_ring_border_color_key = NULL;

//! RING_BORDER_COLOR_KEY specifies the border color of the ring after a keypress.
#define CONFIG_RING_BORDER_COLOR_KEY (cached_ring_border_color_key == NULL ? cached_ring_border_color_key = config_get_color("XRINGLOCK_RING_BORDER_COLOR_KEY", (0x8b/255.0), (0xc3/255.0), (0x4a/255.0), 1.0) : cached_ring_border_color_key)

static color_t *cached_ring_border_color_back = NULL;

//! RING_BORDER_COLOR_BACK specifies the border color of the ring after a backspace keypress.
#define CONFIG_RING_BORDER_COLOR_BACK (cached_ring_border_color_back == NULL ? cached_ring_border_color_back = config_get_color("XRINGLOCK_RING_BORDER_COLOR_BACK", (0xff/255.0), (0x98/255.0), (0x00/255.0), 1.0) : cached_ring_border_color_back)

static color_t *cached_ring_border_color_clear = NULL;

//! RING_BORDER_COLOR_CLEAR specifies the border color of the ring after a clear keypress.
#define CONFIG_RING_BORDER_COLOR_CLEAR (cached_ring_border_color_clear == NULL ? cached_ring_border_color_clear = config_get_color("XRINGLOCK_RING_BORDER_COLOR_CLEAR", (0xf4/255.0), (0x43/255.0), (0x36/255.0), 1.0) : cached_ring_border_color_clear)

static color_t *cached_ring_border_color_auth = NULL;

//! RING_BORDER_COLOR_AUTH specifies the border color of the ring during authentication.
#define CONFIG_RING_BORDER_COLOR_AUTH (cached_ring_border_color_auth == NULL ? cached_ring_border_color_auth = config_get_color("XRINGLOCK_RING_BORDER_COLOR_AUTH", (0x3f/255.0), (0x51/255.0), (0xb5/255.0), 1.0) : cached_ring_border_color_auth)

static color_t *cached_ring_border_color_caps = NULL;

//! RING_BORDER_COLOR_CAPS specifies the border color of the ring if caps lock is enabled.
#define CONFIG_RING_BORDER_COLOR_CAPS (cached_ring_border_color_caps == NULL ? cached_ring_border_color_caps = config_get_color("XRINGLOCK_RING_BORDER_COLOR_CAPS", 0, 0, 0, 0) : cached_ring_border_color_caps)

static double cached_ring_border_width = NAN;

//! RING_BORDER_WIDTH specifies the stroke width of the ring.
#define CONFIG_RING_BORDER_WIDTH (isnan(cached_ring_border_width) ? cached_ring_border_width = config_get_double("XRINGLOCK_RING_BORDER_WIDTH", 10.0) : cached_ring_border_width)

static char cached_show_clear_for_empty = -1;

//! SHOW_CLEAR_FOR_EMPTY specifies whether to show the clear color when pressing backspace even though the password field is already empty.
#define CONFIG_SHOW_CLEAR_FOR_EMPTY (cached_show_clear_for_empty == -1 ? cached_show_clear_for_empty = config_get_bool("XRINGLOCK_SHOW_CLEAR_FOR_EMPTY", true) : cached_show_clear_for_empty)

//! WALLPAPER specifies the wallpaper to use in the lock screen.
//! 
//! NOTE: it is separate from the saver which will be displayed when the prompt is not visible.
#define CONFIG_WALLPAPER config_get_str("XRINGLOCK_WALLPAPER", NULL)

static color_t *cached_background_color = NULL;

//! BACKGROUND_COLOR specifies the background color shown when no wallpaper is available.
#define CONFIG_BACKGROUND_COLOR (cached_background_color == NULL ? cached_background_color = config_get_color("XRINGLOCK_BACKGROUND_COLOR", (0x00/255.0), (0x00/255.0), (0x00/255.0), 1.0) : cached_background_color)

static long cached_framerate = LONG_MIN;

//! FRAMERATE specifies the framerate to run at during animations.
#define CONFIG_FRAMERATE (cached_framerate == LONG_MIN ? cached_framerate = config_get_long("XRINGLOCK_FRAMERATE", 60) : cached_framerate)

static double cached_key_easing_duration = NAN;

//! KEY_EASING_DURATION specifies the duration in milliseconds during which the color fades to normal after a key press.
#define CONFIG_KEY_EASING_DURATION (isnan(cached_key_easing_duration) ? cached_key_easing_duration = config_get_dur("XRINGLOCK_KEY_EASING_DURATION", 250) : cached_key_easing_duration)

static long cached_normal_timeout = LONG_MIN;

//! NORMAL_TIMEOUT specifies the duration in seconds after which the prompt exits when no password was entered.
#define CONFIG_NORMAL_TIMEOUT (cached_normal_timeout == LONG_MIN ? cached_normal_timeout = config_get_long("XRINGLOCK_NORMAL_TIMEOUT", 120) : cached_normal_timeout)

static long cached_password_timeout = LONG_MIN;

//! PASSWORD_TIMEOUT specifies the duration in seconds after which the prompt exits when a password is being entered.
#define CONFIG_PASSWORD_TIMEOUT (cached_password_timeout == LONG_MIN ? cached_password_timeout = config_get_long("XRINGLOCK_PASSWORD_TIMEOUT", 60) : cached_password_timeout)

static const easing_t *cached_key_easing_curve = NULL;

//! KEY_EASING_CURVE specifies the easing curve.
//! 
//! A complete list of easing curves can be found in the man page.
#define CONFIG_KEY_EASING_CURVE (cached_key_easing_curve == NULL ? cached_key_easing_curve = config_get_easing("XRINGLOCK_KEY_EASING_CURVE", "sine_ease_out") : cached_key_easing_curve)

static double cached_key_ring_angle = NAN;

//! KEY_RING_ANGLE specifies the angle of the ring that will be colored.
#define CONFIG_KEY_RING_ANGLE (isnan(cached_key_ring_angle) ? cached_key_ring_angle = config_get_angle("XRINGLOCK_KEY_RING_ANGLE", 60) : cached_key_ring_angle)

static long cached_preferred_monitor = LONG_MIN;

//! PREFERRED_MONITOR specifies the preferred monitor for the prompt.
#define CONFIG_PREFERRED_MONITOR (cached_preferred_monitor == LONG_MIN ? cached_preferred_monitor = config_get_long("XRINGLOCK_PREFERRED_MONITOR", 0) : cached_preferred_monitor)

static char cached_ignore_empty = -1;

//! IGNORE_EMPTY specifies whether to ignore empty passwords.
#define CONFIG_IGNORE_EMPTY (cached_ignore_empty == -1 ? cached_ignore_empty = config_get_bool("XRINGLOCK_IGNORE_EMPTY", false) : cached_ignore_empty)

//! XSECURELOCK_AUTHPROTO specifies the desired authentication protocol module as the name of a file in $libexecdir/xsecurelock.
//! 
//! Names must start with authproto_
#define CONFIG_XSECURELOCK_AUTHPROTO config_get_str("XSECURELOCK_AUTHPROTO", "authproto_pam")

static long cached_xsecurelock_auth_timeout = LONG_MIN;

//! XSECURELOCK_AUTH_TIMEOUT specifies the time (in seconds) to wait for response to a prompt before giving up and reverting to the screen saver.
//! 
//! use {normal,password}_timeout instead
//! 
//! TODO: not yet implemented
#define CONFIG_XSECURELOCK_AUTH_TIMEOUT (cached_xsecurelock_auth_timeout == LONG_MIN ? cached_xsecurelock_auth_timeout = config_get_long("XSECURELOCK_AUTH_TIMEOUT", 300) : cached_xsecurelock_auth_timeout)

static char cached_xsecurelock_show_datetime = -1;

//! XSECURELOCK_SHOW_DATETIME specifies whether to show local date and time on the login.
//! 
//! TODO: not yet implemented
#define CONFIG_XSECURELOCK_SHOW_DATETIME (cached_xsecurelock_show_datetime == -1 ? cached_xsecurelock_show_datetime = config_get_bool("XSECURELOCK_SHOW_DATETIME", 0) : cached_xsecurelock_show_datetime)

//! XSECURELOCK_DATETIME_FORMAT specifies the date format to show. (see strftime(3))
//! 
//! TODO: not yet implemented
#define CONFIG_XSECURELOCK_DATETIME_FORMAT config_get_str("XSECURELOCK_DATETIME_FORMAT", NULL)

static char cached_xsecurelock_show_hostname = -1;

//! XSECURELOCK_SHOW_HOSTNAME specifies whether to show the hostname on the login screen.
//! 
//! Possible values are 0 for not showing the hostname, 1 for showing the short form, and 2 for showing the long form.
#define CONFIG_XSECURELOCK_SHOW_HOSTNAME (cached_xsecurelock_show_hostname == -1 ? cached_xsecurelock_show_hostname = config_get_bool("XSECURELOCK_SHOW_HOSTNAME", 0) : cached_xsecurelock_show_hostname)

static char cached_xsecurelock_show_username = -1;

//! XSECURELOCK_SHOW_USERNAME specifies whether to show the username on the login screen.
#define CONFIG_XSECURELOCK_SHOW_USERNAME (cached_xsecurelock_show_username == -1 ? cached_xsecurelock_show_username = config_get_bool("XSECURELOCK_SHOW_USERNAME", 0) : cached_xsecurelock_show_username)

//! XSECURELOCK_FONT specifies a X11 or FontConfig font name to use for auth_x11. You can get a list of supported font names by running xlsfonts and fc-list.
//! 
//! TODO: not yet implemented
#define CONFIG_XSECURELOCK_FONT config_get_str("XSECURELOCK_FONT", NULL)

//! XSCREENSAVER_WINDOW is set by xsecurelock.
#define CONFIG_XSCREENSAVER_WINDOW config_get_ull("XSCREENSAVER_WINDOW")

