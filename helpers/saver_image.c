#include <stdlib.h>
#include <signal.h>
#include <cairo/cairo.h>

#include <version.h>

#include "backend.h"
#include "logging.h"
#include "cairo_utils.h"

static void suspend(backend_t* b);

int main(int argc, char **argv) {
    if (argc >= 2) {
        if (strcmp(argv[1], "--help") == 0) {
            printf("saver_image [--help]|[--version]\n\nGive either XRINGLOCK_SAVER_WALLPAPER or XRINGLOCK_WALLPAPER through the environment.\n");
            return 1;
        } else if (strcmp(argv[1], "--version") == 0) {
            printf("saver_image v" XRINGLOCK_VERSION "\n\nPart of https://gitlab.com/Ma_124/xringlock\n");
            return 1;
        }
    }

    backend_t *b = backend_new();
    cairo_t *cr = NULL;
    cairo_surface_t *wallpaper = NULL;

    int w, h;
    cairo_surface_t *surface = backend_get_surface(b, &w, &h);
    if (surface == NULL) {
        Log("could not acquire cairo surface");
        goto free_and_suspend;
    }

    cr = cairo_create(surface);
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
    char *f = getenv("XRINGLOCK_SAVER_WALLPAPER");
    if (f == NULL || f[0] == '\0') {
        f = getenv("XRINGLOCK_WALLPAPER");
        if (f == NULL || f[0] == '\0') {
            Log("either XRINGLOCK_SAVER_WALLPAPER or XRINGLOCK_WALLPAPER must be set");
            goto free_and_suspend;
        }
    }

    wallpaper = load_image(f, w, h);
    if (wallpaper == NULL) {
        Log("could not load wallpaper");
        goto free_and_suspend;
    }

    cairo_set_source_surface(cr, wallpaper, 0, 0);
    cairo_paint(cr);

    cairo_surface_flush(surface);
    backend_flush(b);

free_and_suspend:
    cairo_destroy(cr);
    cairo_surface_destroy(wallpaper);

    suspend(b);
}

static void suspend(backend_t* b) {
    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGINT);
    sigaddset(&mask, SIGTERM);
    sigaddset(&mask, SIGQUIT);
    sigaddset(&mask, SIGABRT);
    sigaddset(&mask, SIGIOT);
    sigaddset(&mask, SIGBUS);
    sigsuspend(&mask);
    backend_destroy(b);
    exit(0);
}
