/*
 * Copyright 2014 Google Inc. All rights reserved.
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <wait_pgrp.h>
#include <helpers/authproto.h>

#include "auth.h"
#include "logging.h"
#include "config.h"

struct authproto {
    int reqfd, respfd;
    pid_t childpid;
};

authproto_t* authproto_new() {
    int requestfd[2], responsefd[2];
    if (pipe(requestfd)) {
        LogErrno("pipe");
        return NULL;
    }
    if (pipe(responsefd)) {
        LogErrno("pipe");
        return NULL;
    }

    // Use authproto_pam.
    pid_t childpid = ForkWithoutSigHandlers();
    if (childpid == -1) {
        LogErrno("fork");
        return NULL;
    }

    if (childpid == 0) {
        // Child process. Just run authproto_pam.
        // But first, move requestfd[1] to 1 and responsefd[0] to 0.
        close(requestfd[0]);
        close(responsefd[1]);

        if (requestfd[1] == 0) {
            // Tricky case. We don't _expect_ this to happen - after all,
            // initially our own fd 0 should be bound to xsecurelock's main
            // program - but nevertheless let's handle it.
            // At least this implies that no other fd is 0.
            int requestfd1 = dup(requestfd[1]);
            if (requestfd1 == -1) {
                LogErrno("dup");
                _exit(EXIT_FAILURE);
            }
            close(requestfd[1]);
            if (dup2(responsefd[0], 0) == -1) {
                LogErrno("dup2");
                _exit(EXIT_FAILURE);
            }
            close(responsefd[0]);
            if (requestfd1 != 1) {
                if (dup2(requestfd1, 1) == -1) {
                    LogErrno("dup2");
                    _exit(EXIT_FAILURE);
                }
                close(requestfd1);
            }
        } else {
            if (responsefd[0] != 0) {
                if (dup2(responsefd[0], 0) == -1) {
                    LogErrno("dup2");
                    _exit(EXIT_FAILURE);
                }
                close(responsefd[0]);
            }
            if (requestfd[1] != 1) {
                if (dup2(requestfd[1], 1) == -1) {
                    LogErrno("dup2");
                    _exit(EXIT_FAILURE);
                }
                close(requestfd[1]);
            }
        }
        {
            const char *args[2] = {CONFIG_XSECURELOCK_AUTHPROTO, NULL};
            ExecvHelper(CONFIG_XSECURELOCK_AUTHPROTO, args);
            sleep(2);  // Reduce log spam or other effects from failed execv.
            _exit(EXIT_FAILURE);
        }
    }

    // Otherwise, we're in the parent process.
    close(requestfd[1]);
    close(responsefd[0]);

    authproto_t *conn = malloc(sizeof(*conn));
    conn->childpid = childpid;
    conn->reqfd = requestfd[0];
    conn->respfd = responsefd[1];
    return conn;
}

bool authproto_destroy(authproto_t* conn) {
    close(conn->reqfd);
    close(conn->respfd);
    int status;
    if (!WaitProc("authproto", &conn->childpid, 1, 0, &status)) {
        Log("WaitPgrp returned false but we were blocking");
        abort();
    }
    free(conn);
    return status == 0;
}

char authproto_receive_message(authproto_t* conn, char **msg) {
    return ReadPacket(conn->reqfd, msg, true);
}

void authproto_send_message(authproto_t* conn, char type, char *msg) {
    WritePacket(conn->respfd, type, msg);
}
