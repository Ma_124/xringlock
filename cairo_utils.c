/*
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>
#include <cairo/cairo.h>
#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <stdbool.h>

#include "cairo_utils.h"
#include "logging.h"

static color_t transparent_color = {0, 0.0, 0.0, 0.0, 1.0};

color_t *new_color() {
    color_t *c = malloc(sizeof(*c));
    c->flags = 0;
    c->a = 1;
    return c;
}

color_t *new_randomized_color() {
    color_t *c = malloc(sizeof(*c));
    c->flags |= (unsigned char) 1;
    return c;
}

void cairo_set_source_color(cairo_t *cr, color_t *color) {
    if (color->flags & (unsigned char) 1) {
        color->r = drand48();
        color->g = drand48();
        color->b = drand48();
        cairo_set_source_rgb(cr, color->r, color->g, color->b);
    } else
        cairo_set_source_rgba(cr, color->r, color->g, color->b, color->a);
}

void cairo_set_source_color_with_alpha(cairo_t *cr, color_t *color, double alpha) {
    cairo_set_source_rgba(cr, color->r, color->g, color->b, color->a*alpha);
}

cairo_surface_t *load_image(const char *filepath, int width, int height) {
    if (filepath == NULL)
        return NULL;

    GError *err = NULL;
    // TODO better scaling/fitting
    GdkPixbuf *buf = gdk_pixbuf_new_from_file_at_scale(filepath, -1, height, true, &err);
    if (err != NULL) {
        Log("could not load image %s: %s", filepath, err->message);
        g_error_free(err);
        return NULL;
    }

    cairo_format_t format = (gdk_pixbuf_get_has_alpha(buf)) ? CAIRO_FORMAT_ARGB32 : CAIRO_FORMAT_RGB24;
    cairo_surface_t *surface = cairo_image_surface_create(format, width, height);
    cairo_t *cr = cairo_create(surface);
    gdk_cairo_set_source_pixbuf(cr, buf, 0, 0);
    cairo_paint(cr);
    cairo_destroy(cr);
    g_object_unref(buf);

    return surface;
}
