/*
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <stdbool.h>

#ifndef MAX_PW_LEN
#define MAX_PW_LEN 255
#endif

#define EVENT_SOURCE_CRASH     0
#define EVENT_SOURCE_AUTHPROTO 1
#define EVENT_SOURCE_BACKEND   2

struct state;
typedef struct state state_t;

state_t* state_new();
void state_destroy(state_t* st);

void state_set_event_source(state_t *st, char src);
char state_get_event_source(state_t *st);

double state_get_width(state_t *st);
void state_set_width(state_t *st, double w);

double state_get_height(state_t *st);
void state_set_height(state_t *st, double h);

char *state_get_user(state_t *st);
void state_set_user(state_t *st, char *user);
char *state_get_pw(state_t *st);

bool state_pw_append(state_t* st, char b);
bool state_pw_back(state_t* st);
void state_pw_clear(state_t* st);

void state_set_caps(state_t* st, bool caps);
bool state_has_caps(state_t* st);
