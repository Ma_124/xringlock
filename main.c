/*
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <wait_pgrp.h>
#include <version.h>

#include "config.h"
#include "loop.h"
#include "logging.h"

#define XSL_AUTH "XSECURELOCK_AUTH="

// TODO add compilation option for BSDs to specify their own proc file
// FreeBSD (if /proc installed): /proc/curproc/file
// {Free,DragonFly }BSD (if /proc installed): /proc/curproc/file
// NetBSD: /proc/curproc/exe
// if compiled with github.com/gpakosz/whereami support
// else fallback on argv[0]
#ifndef PROC_SELF_EXE
#ifndef __gnu_linux__
#warning "your OS needs procfs support. by default this program looks up /proc/self/exe. contact your packager or the maintainers"
#endif

#define PROC_SELF_EXE "/proc/self/exe"
#endif

int main(int argc, char **argv) {
    if (config_get_str("XSCREENSAVER_WINDOW", NULL) == NULL) {
        if (argc >= 2) {
            if (strcmp(argv[1], "--help") == 0) {
                printf("usage: auth_xringlock [-- command to run when locked]|[--version]\n\nMost configuration is done through environment variables. See xringlock(1)\n");
                // only ever exit with 0 if authproto was successful
                return 1;
            } else if (strcmp(argv[1], "--version") == 0) {
                printf("xringlock v" XRINGLOCK_VERSION "\n");
                return 1;
            }
        }
        char xsecurelock_auth_env[sizeof(XSL_AUTH) + PATH_MAX] = XSL_AUTH;
        if (realpath(PROC_SELF_EXE, &xsecurelock_auth_env[sizeof(XSL_AUTH)-1]) == NULL) {
            LogErrno("could not find absolute path to myself");
            return 255;
        }
        argv[0] = "xsecurelock";
        putenv(xsecurelock_auth_env);
        Trace("execvp");
        execvp("xsecurelock", argv);
        LogErrno("execvp xsecurelock error");
        return 1;
    }

    InitWaitPgrp();
    return main_loop();
}
