/*
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <unistd.h>
#include <malloc.h>
#include <helpers/authproto.h>
#include <time.h>

#include "loop.h"
#include "state.h"
#include "backend.h"
#include "logging.h"
#include "config.h"
#include "auth.h"
#include "indicator.h"

#define SECOND_IN_NANOS 1000000000

static bool is_animating = false;

struct timespec sleep_time = {0, 0};
struct timespec normal_timeout = {0, 0};
struct timespec password_timeout = {0, 0};

static bool handle_key(state_t *st, char key, char *out_event_type);

static bool
handle_authproto(state_t *st, authproto_t *conn, char authproto_type, char *authproto_message, char *out_event_type);

int main_loop() {
    Trace("entering");

    // start authproto handler or fail
    authproto_t *conn = authproto_new();
    if (conn == NULL) {
        Log("failed to open authproto conn");
        return 1;
    }

    // initialize state and rendering backend
    int exit_status;
    state_t *st = state_new();
    backend_t *b = backend_new();
    Trace("initialized state");

    // initialize indicator
    indicator_t in;
    indicator_state_t *inst = indicator_new("ring", &in);

    // counts how many frames have elapsed since now
    // overflows after 2.3 years of continues animation at 60 fps
    // and even then only causes a small graphical blip
    unsigned long time = 0;

    // set sleep_time according to configured framerate
    sleep_time.tv_nsec = SECOND_IN_NANOS/CONFIG_FRAMERATE;

    normal_timeout.tv_sec = CONFIG_NORMAL_TIMEOUT;
    password_timeout.tv_sec = CONFIG_PASSWORD_TIMEOUT;

    // get a cairo surface and it's dimensions
    int width, height;
    cairo_surface_t *surface = backend_get_surface(b, &width, &height);
    if (surface == NULL) {
        Log("could not acquire cairo surface");
        return 255;
    }
    Trace("got surface");
    state_set_width(st, width);
    state_set_height(st, height);

    cairo_t *cr = cairo_create(surface);

    // load the wallpaper into memory
    // returns NULL if none was found
    cairo_surface_t *wallpaper = load_image(CONFIG_WALLPAPER, width, height);
    Trace("got wallpaper");

    // allocate variables for use in events
    char event_type = EVENT_TYPE_INIT;
    char key;
    char authproto_type;
    char *authproto_message = NULL;

    for (;;) {
        // check if any errors occurred during the last draw
        int cr_status = cairo_status(cr);
        if (cr_status != 0) {
            Log("cairo status: %d %s", cr_status, cairo_status_to_string(cr_status));
        }

        state_set_caps(st, backend_caps(b));

        // store all drawing operations on new group
        // this prevents X from displaying the surface while we're still drawing
        cairo_push_group(cr);

        // replace everything currently drawn with the wallpaper or background color
        cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
        if (wallpaper != NULL)
            cairo_set_source_surface(cr, wallpaper, 0, 0);
        else
            cairo_set_source_color(cr, CONFIG_BACKGROUND_COLOR);
        cairo_paint(cr);
        Trace("painted wallpaper/background color");

        // reset operator to default
        cairo_set_operator(cr, CAIRO_OPERATOR_OVER);

        // draw the current state
        indicator_draw(in, inst, st, cr, time++, event_type);
        Trace("drew ring");

        // apply all drawing operations
        // for more context see cairo_push_group
        cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
        cairo_pop_group_to_source(cr);
        cairo_paint(cr);

        // flush the contents of the surface
        cairo_surface_flush(surface);
        backend_flush(b);
        Trace("popped and flushed surface");

        switch (state_get_event_source(st)) {
            case EVENT_SOURCE_AUTHPROTO:
                Trace("event_source EVENT_SOURCE_AUTHPROTO");
                // receive event from authproto handler
                authproto_type = authproto_receive_message(conn, &authproto_message);
                bool authproto_status = handle_authproto(st, conn, authproto_type, authproto_message, &event_type);

                Trace("free message");
                // delete message
                if (authproto_message != NULL) {
                    explicit_bzero(authproto_message, strlen(authproto_message));
                    free(authproto_message);
                    authproto_message = NULL;
                }

                if (!authproto_status) {
                    Trace("authproto exit");
                    exit_status = 1;
                    goto done;
                }
                Trace("authproto continue");
                break;
            case EVENT_SOURCE_BACKEND:
                Trace("event_source EVENT_SOURCE_BACKEND");
                if (indicator_is_animating(in, inst, time) != is_animating) {
                    is_animating = !is_animating;
                    if (is_animating)
                        backend_unset_next_blocks(b);
                    else
                        backend_set_next_blocks(b);
                }
                // read next key from stdin
                // or set key = 0 if there was no new input
                char code = backend_next_key(b, &key, state_get_pw(st)[0] == '\0' ? &normal_timeout : &password_timeout);
                if (code == true) {
                    if (handle_key(st, key, &event_type)) {
                        // enter was pressed
                        if (state_get_pw(st)[0] == '\0' && CONFIG_IGNORE_EMPTY) {
                            event_type = EVENT_TYPE_RENDER;
                            continue;
                        }

                        Trace("authproto submit password");
                        // send password
                        authproto_send_message(conn, PTYPE_RESPONSE_LIKE_PASSWORD, state_get_pw(st));
                        state_pw_clear(st);
                        Trace("authproto password submitted");

                        // return control to authproto
                        state_set_event_source(st, EVENT_SOURCE_AUTHPROTO);

                        // set event type for redrawing
                        event_type = EVENT_TYPE_PAM;
                        Trace("backend return control");
                    } else
                        Trace("backend continue");
                } else if (code > 100) {
                    exit_status = 254;
                    goto done;
                } else
                    event_type = EVENT_TYPE_RENDER;
                break;
            default:
                Log("unknown event source %02x", state_get_event_source(st));
                exit_status = 255;
                goto done;
        }

        nanosleep(&sleep_time, NULL);
    }

    done:
    Trace("done");

    // destroy and free all state
    cairo_destroy(cr);
    cairo_surface_destroy(surface);
    cairo_surface_destroy(wallpaper);
    indicator_destroy(in, inst);

    backend_destroy(b);
    state_destroy(st);

    // returns true if authproto handler exited with 0 which means successful authentication
    if (authproto_destroy(conn)) {
        exit_status = 0;
    }

    Trace("exiting");
    return exit_status;
}


static bool handle_key(state_t *st, char key, char *out_event_type) {
    switch (key) {
        case '\b':    // BACKSPACE
        case '\177':  // DEL
            if (state_pw_back(st) || !CONFIG_SHOW_CLEAR_FOR_EMPTY)
                *out_event_type = EVENT_TYPE_BACK;
            else
                *out_event_type = EVENT_TYPE_CLEAR; // last character was reached
            break;
        case '\033': // ESC
        case '\001': // C-A
        case '\025': // C-U
            state_pw_clear(st);
            *out_event_type = EVENT_TYPE_CLEAR;
            break;
        case '\r':
        case '\n':   // RETURN
            // here the event type is set by the caller
            return true;
        default:
            // discard other ASCII control characters
            if (key >= '\000' && key <= '\037') {
                *out_event_type = EVENT_TYPE_RENDER;
                break;
            }
            // append all non-control characters
            state_pw_append(st, key);
            *out_event_type = EVENT_TYPE_KEY;
    }
    return false;
}

bool
handle_authproto(state_t *st, authproto_t *conn, char authproto_type, char *authproto_message, char *out_event_type) {
    // TODO add more event types
    char *uname;
    switch (authproto_type) {
        case PTYPE_INFO_MESSAGE:
            Log("auth helper says: %s", authproto_message);
            break;
        case PTYPE_ERROR_MESSAGE:
            Log("auth helper error: %s", authproto_message);
            break;
        case PTYPE_PROMPT_LIKE_USERNAME:
            uname = state_get_user(st);
            if (uname != NULL) {
                authproto_send_message(conn, PTYPE_RESPONSE_LIKE_USERNAME, uname);
            } else {
                authproto_send_message(conn, PTYPE_RESPONSE_CANCELLED, "");
            }
            break;
        case PTYPE_PROMPT_LIKE_PASSWORD:
            // main_loop will now read the password and then call authproto_send_message
            state_set_event_source(st, EVENT_SOURCE_BACKEND);
            *out_event_type = EVENT_TYPE_INIT;
            break;
        case 0:
            Trace("eof");
            // no event read
            return false;
        default:
            Log("unknown message type %02x", (int) authproto_type);
            return false;
    }
    return true;
}
