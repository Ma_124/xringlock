/*
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "util.h"

// HELPER_PATH does not fit in this header but is required right here
// because this is the only file in this project that is included by
// wait_pgrp.c which is taken straight from xsecurelock and which we
// therefore can't edit
#ifndef HELPER_PATH
#define HELPER_PATH "/usr/lib/xsecurelock/"
#endif

void log_prefix();

#ifdef DEBUG
#define Log(format, ...) do { \
    log_prefix(); \
    fprintf(stderr, "%40s:%03d %-30s  " format "\n", __FILE__+SOURCE_PATH_SIZE, __LINE__, __func__, ##__VA_ARGS__); \
} while(0)
#else
#define Log(format, ...) do { \
    log_prefix(); \
    fprintf(stderr, format "\n", ##__VA_ARGS__); \
} while(0)
#endif

#define LogErrno(format, ...) Log(format ": errno: %d %s", errno, strerror(errno), ##__VA_ARGS__)

#if defined(DEBUG) && defined(TRACE)
#define Trace(format, ...) Log(format, ##__VA_ARGS__)
#else
#define Trace(format, ...) do {} while(0)
#endif
