/*
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <stdbool.h>
#include <math.h>
#include <limits.h>

#include "cairo_utils.h"
#include "easings.h"

#include "config_gen.h"

const char *config_get_str(const char *name, const char* def);
long config_get_long(const char *name, long def);
double config_get_double(const char *name, double def);
double config_get_dur(const char *name, double def);
double config_get_angle(const char *name, double def);
const easing_t *config_get_easing(const char *name, const char *def);
color_t *config_get_color(const char *name, double def_r, double def_g, double def_b, double def_a);
unsigned long long config_get_ull(const char *name);
bool config_get_bool(const char *name, bool def);
