/*
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include <wait_pgrp.h>
#include <mlock_page.h>

#include "state.h"
#include "logging.h"

struct state {
    char event_source;

    double width, height;

    char *user_name;
    char pw_buf[MAX_PW_LEN + 1];
    int pw_len;

    bool caps;
};

state_t *state_new() {
    struct state *st = malloc(sizeof(*st));
    if (MLOCK_PAGE(st->pw_buf, MAX_PW_LEN) < 0)
        Log("could not lock password buffer. password could have been stored to swap");
    st->event_source = EVENT_SOURCE_AUTHPROTO;
    st->user_name = getenv("USER");
    st->pw_len = 0;
    st->caps = false;
    return st;
}

char *state_get_user(state_t *st) {
    return st->user_name;
}

void state_set_event_source(state_t *st, char src) {
    st->event_source = src;
}

char state_get_event_source(state_t *st) {
    return st->event_source;
}

double state_get_width(state_t *st) {
    return st->width;
}


void state_set_width(state_t *st, double w) {
    st->width = w;
}

double state_get_height(state_t *st) {
    return st->height;
}


void state_set_height(state_t *st, double h) {
    st->height = h;
}

void state_set_user(state_t *st, char *user) {
    free(st->user_name);
    st->user_name = user;
}

char *state_get_pw(state_t *st) {
    // add null terminator
    st->pw_buf[st->pw_len] = 0;
    return st->pw_buf;
}

void state_destroy(state_t *st) {
    if (st == NULL)
        return;
    explicit_bzero(st->pw_buf, MAX_PW_LEN);
    for (int i = 0; i < MAX_PW_LEN; i++)
        st->pw_buf[i] = 0;
    free(st);
}

bool state_pw_append(state_t *st, char b) {
    if (st->pw_len < MAX_PW_LEN) {
        st->pw_buf[st->pw_len++] = b;
        return true;
    } else {
        Log("password was too long (max %d). recompile with greater MAX_PW_LEN", MAX_PW_LEN);
        st->pw_len = 0;
        return false;
    }
}

bool state_pw_back(state_t *st) {
    if (st->pw_len <= 1) {
        st->pw_len = 0;
        return false;
    }

    // reset internal shift state
    mblen(NULL, 0);

    int pos = 0;
    int prev_pos = 0;
    while (pos < st->pw_len) {
        prev_pos = pos;

        // length of current rune
        int len = mblen(st->pw_buf + pos, st->pw_len - pos);
        if (len <= 0)
            break;

        pos += len;
    }
    st->pw_len = prev_pos;
    return true;
}

void state_pw_clear(state_t *st) {
    st->pw_len = 0;
}

void state_set_caps(state_t *st, bool caps) {
    st->caps = caps;
}

bool state_has_caps(state_t *st) {
    return st->caps;
}
