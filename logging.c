/*
 * Copyright 2014 Google Inc. All rights reserved.
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include "logging.h"

void log_prefix() {
    int old_errno = errno;
    time_t t = time(NULL);
    struct tm tm_buf;
    struct tm *tm = gmtime_r(&t, &tm_buf);
    char s[32];
    if (!strftime(s, sizeof(s), "%Y-%m-%dT%H:%M:%SZ ", tm)) {
        *s = 0;
    }
    fprintf(stderr, "%s%ld xringlock:   ", s, (long)getpid());
    errno = old_errno;
}
