/*
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "config.h"

#include "util.h"
#include "logging.h"

//! config_get_str loads a string from the config with key name and default def
const char *config_get_str(const char *name, const char *def) {
    Trace("config access (str): %s", name);
    char *ch = getenv(name);
    if (ch == NULL || ch[0] == 0)
        return def;
    return ch;
}

//! config_get_str loads a long from the config with key name and default def in case of parsing errors.
long config_get_long(const char *name, long def) {
    Trace("config access: %s", name);
    const char *ch = config_get_str(name, NULL);
    if (ch == NULL)
        return def;
    char *firstUnexpected;
    long v = strtol(ch, &firstUnexpected, 10);
    if (*firstUnexpected != 0) {
        // invalid number was passed
        Log("%s: the invalid character '%c' was found in the integer literal. default %ld is used.\n", name,
            *firstUnexpected, def);
        return def;
    }
    return v;
}

//! config_get_double loads a double from the config with key name and default def in case of parsing errors.
double config_get_double(const char *name, double def) {
    Trace("config access: %s", name);
    const char *ch = config_get_str(name, NULL);
    if (ch == NULL)
        return def;
    char *firstUnexpected;
    double v = strtod(ch, &firstUnexpected);
    if (*firstUnexpected != 0) {
        // invalid number was passed
        Log("%s: the invalid character '%c' was found in the double literal. default %.2f is used.\n", name,
            *firstUnexpected, def);
        return def;
    }
    return v;
}

double config_get_dur(const char *name, double def) {
    Trace("config access: %s", name);
    return (config_get_double(name, def)/1000)*CONFIG_FRAMERATE;
}

double config_get_angle(const char *name, double def) {
    Trace("config access: %s", name);
    return (config_get_double(name, def)/180.0)*M_PI;
}

//! config_get_color loads a color from the config with key name and default def_{r,g,b,a} in case of parsing errors into out.
color_t *config_get_color(const char *name, double def_r, double def_g, double def_b, double def_a) {
    Trace("config access: %s", name);

    color_t *out = new_color();
    const char *ch = config_get_str(name, NULL);
    if (ch == NULL)
        goto return_default;

    unsigned long len = strlen(ch);

    // check for magic values
#define ifstreq(str) if (sizeof(str)-1 == len && strncasecmp(ch, str, sizeof(str)-1) == 0)

    ifstreq("none") {
        out->r = 0;
        out->g = 0;
        out->b = 0;
        out->a = 0;
        return out;
    }

    ifstreq("rand") {
        out->flags |= (unsigned char) 1;
        return out;
    }

#undef ifstreq

    Trace("parse color: not a constant");

    out->a = 1.0;

#define fail_parse(format, ...) do { \
    Log("%s: the color literal %s could not be parsed. " format " default %.2f, %.2f, %.2f, %.2f is used.", \
    name, ch, ##__VA_ARGS__, def_r, def_g, def_b, def_a); goto return_default;                                \
} while(0)

#define assert_hex(off) do { \
if ((ch[off] < '0' || ch[off] > '9') && \
    (ch[off] < 'a' || ch[off] > 'f') && \
    (ch[off] < 'A' || ch[off] > 'F'))   \
        fail_parse("invalid character '%c' in hex literal. must be one of [0-9A-Fa-f]", ch[off]); \
} while (0)

#define nibbles(lower, upper) ((parse_hex_char(lower) << ((unsigned char) 4)) | parse_hex_char(upper))

#pragma clang diagnostic push
#pragma ide diagnostic ignored "hicpp-signed-bitwise"
    if (ch[0] == '#')
        switch (len - 1) {
            case 4 * 1:
                // #RGBA
                assert_hex(4);
                out->a = nibbles(ch[4], ch[4]) / 255.0;
                // fallthrough
            case 3 * 1:
                // #RGB
                assert_hex(1);
                assert_hex(2);
                assert_hex(3);
                out->r = nibbles(ch[1], ch[1]) / 255.0;
                out->g = nibbles(ch[2], ch[2]) / 255.0;
                out->b = nibbles(ch[3], ch[3]) / 255.0;
                return out;
            case 4 * 2:
                // #RRGGBBAA
                assert_hex(7);
                assert_hex(8);
                out->a = nibbles(ch[7], ch[8]) / 255.0;
            case 3 * 2:
                // #RRGGBB
                assert_hex(1);
                assert_hex(2);
                assert_hex(3);
                assert_hex(4);
                assert_hex(5);
                assert_hex(6);
                out->r = nibbles(ch[1], ch[2]) / 255.0;
                out->g = nibbles(ch[3], ch[4]) / 255.0;
                out->b = nibbles(ch[5], ch[6]) / 255.0;
                return out;
            default:
                fail_parse("hex color literals must match /#RR?GG?BB?(AA?)?/");
        }

    fail_parse("");

#pragma clang diagnostic pop
#undef nibbles
#undef assert_hex
#undef fail_parse

    return_default:
    Trace("parse color: default");
    if (isnan(def_r)) {
        out->flags |= (unsigned char) 1;
        Trace("parse color: return random");
        return out;
    }

    out->r = def_r;
    out->g = def_g;
    out->b = def_b;
    out->a = def_a;

    Trace("parse color: return default");
    return out;
}

//! config_get_str loads an unsigned unsigned long from the config with key name and default 0 in case of parsing errors.
unsigned long long config_get_ull(const char *name) {
    Trace("config access: %s", name);
    const char *ch = config_get_str(name, NULL);
    if (ch == NULL)
        return 0;
    char *firstUnexpected;
    unsigned long long v = strtoull(ch, &firstUnexpected, 10);
    if (*firstUnexpected != 0)
        return 0;
    return v;
}

//! config_get_bool loads a boll from the config with key name and default def.
bool config_get_bool(const char *name, bool def) {
    Trace("config access: %s", name);
    const char *ch = config_get_str(name, NULL);
    if (ch == NULL)
        return def;
    unsigned long len = strlen(ch);
    if (len == 1) {
        if (ch[0] == '0')
            return false;
        else if (ch[0] == '1')
            return true;
    } else if (len == sizeof("true")-1) {
        if (strncmp(ch, "true", len) == 0)
            return true;
    } else if (len == sizeof("false")-1) {
        if (strncmp(ch, "false", len) == 0)
            return false;
    }
    Log("unknown boolean value '%s' using default %s", ch, def ? "true" : "false");
    return def;
}

const easing_t *config_get_easing(const char *name, const char *def) {
    Trace("config access: %s", name);
    const char *ch = config_get_str(name, def);
    if (ch == NULL)
        ch = def;
    return easings_lookup(ch);
}
