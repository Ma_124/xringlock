#!/bin/sh

# This shell script runs xringlock.
# The environment variables below contain the default values.

# XRINGLOCK_RING_RADIUS specifies the radius of the ring.
export XRINGLOCK_RING_RADIUS=100.0

# XRINGLOCK_RING_FILL_COLOR specifies the fill color of the ring.
export XRINGLOCK_RING_FILL_COLOR="#333333cc"

# XRINGLOCK_RING_FILL_COLOR_KEY specifies the fill color of the ring after a keypress.
export XRINGLOCK_RING_FILL_COLOR_KEY="none"

# XRINGLOCK_RING_FILL_COLOR_BACK specifies the fill color of the ring after a backspace keypress.
export XRINGLOCK_RING_FILL_COLOR_BACK="none"

# XRINGLOCK_RING_FILL_COLOR_CLEAR specifies the fill color of the ring after a clear keypress.
export XRINGLOCK_RING_FILL_COLOR_CLEAR="none"

# XRINGLOCK_RING_FILL_COLOR_AUTH specifies the fill color of the ring during authentication.
export XRINGLOCK_RING_FILL_COLOR_AUTH="none"

# XRINGLOCK_RING_FILL_COLOR_CAPS specifies the fill color of the ring if caps lock is enabled.
export XRINGLOCK_RING_FILL_COLOR_CAPS="#f44336"

# XRINGLOCK_RING_BORDER_COLOR specifies the border color of the ring.
export XRINGLOCK_RING_BORDER_COLOR="none"

# XRINGLOCK_RING_BORDER_COLOR_KEY specifies the border color of the ring after a keypress.
export XRINGLOCK_RING_BORDER_COLOR_KEY="#8bc34a"

# XRINGLOCK_RING_BORDER_COLOR_BACK specifies the border color of the ring after a backspace keypress.
export XRINGLOCK_RING_BORDER_COLOR_BACK="#ff9800"

# XRINGLOCK_RING_BORDER_COLOR_CLEAR specifies the border color of the ring after a clear keypress.
export XRINGLOCK_RING_BORDER_COLOR_CLEAR="#f44336"

# XRINGLOCK_RING_BORDER_COLOR_AUTH specifies the border color of the ring during authentication.
export XRINGLOCK_RING_BORDER_COLOR_AUTH="#3f51b5"

# XRINGLOCK_RING_BORDER_COLOR_CAPS specifies the border color of the ring if caps lock is enabled.
export XRINGLOCK_RING_BORDER_COLOR_CAPS="none"

# XRINGLOCK_RING_BORDER_WIDTH specifies the stroke width of the ring.
export XRINGLOCK_RING_BORDER_WIDTH=10.0

# XRINGLOCK_SHOW_CLEAR_FOR_EMPTY specifies whether to show the clear color when pressing backspace even though the password field is already empty.
export XRINGLOCK_SHOW_CLEAR_FOR_EMPTY=true

# XRINGLOCK_WALLPAPER specifies the wallpaper to use in the lock screen.
# 
# NOTE: it is separate from the saver which will be displayed when the prompt is not visible.
export XRINGLOCK_WALLPAPER=""

# XRINGLOCK_BACKGROUND_COLOR specifies the background color shown when no wallpaper is available.
export XRINGLOCK_BACKGROUND_COLOR="#000000"

# XRINGLOCK_FRAMERATE specifies the framerate to run at during animations.
export XRINGLOCK_FRAMERATE=60

# XRINGLOCK_KEY_EASING_DURATION specifies the duration in milliseconds during which the color fades to normal after a key press.
export XRINGLOCK_KEY_EASING_DURATION=250

# XRINGLOCK_NORMAL_TIMEOUT specifies the duration in seconds after which the prompt exits when no password was entered.
export XRINGLOCK_NORMAL_TIMEOUT=120

# XRINGLOCK_PASSWORD_TIMEOUT specifies the duration in seconds after which the prompt exits when a password is being entered.
export XRINGLOCK_PASSWORD_TIMEOUT=60

# XRINGLOCK_KEY_EASING_CURVE specifies the easing curve.
# 
# A complete list of easing curves can be found in the man page.
export XRINGLOCK_KEY_EASING_CURVE="sine_ease_out"

# XRINGLOCK_KEY_RING_ANGLE specifies the angle of the ring that will be colored.
export XRINGLOCK_KEY_RING_ANGLE=60

# XRINGLOCK_PREFERRED_MONITOR specifies the preferred monitor for the prompt.
export XRINGLOCK_PREFERRED_MONITOR=0

# XRINGLOCK_IGNORE_EMPTY specifies whether to ignore empty passwords.
export XRINGLOCK_IGNORE_EMPTY=false

# XSECURELOCK_AUTHPROTO specifies the desired authentication protocol module as the name of a file in $libexecdir/xsecurelock.
# 
# Names must start with authproto_
export XSECURELOCK_AUTHPROTO="authproto_pam"

# XSECURELOCK_AUTH_TIMEOUT specifies the time (in seconds) to wait for response to a prompt before giving up and reverting to the screen saver.
# 
# use {normal,password}_timeout instead
# 
# TODO: not yet implemented
export XSECURELOCK_AUTH_TIMEOUT=300

# XSECURELOCK_BLANK_TIMEOUT specifies the time (in seconds) before telling X11 to fully blank the screen; a negative value disables X11 blanking.
export XSECURELOCK_BLANK_TIMEOUT=600

# XSECURELOCK_BLANK_DPMS_STATE specifies which DPMS state to put the screen in when blanking (one of standby, suspend, off and on, where "on" means to not invoke DPMS at all).
export XSECURELOCK_BLANK_DPMS_STATE="off"

# XSECURELOCK_COMPOSITE_OBSCURER specifies whether create a second full-screen window to obscure window content in case a running compositor unmaps its own window.
export XSECURELOCK_COMPOSITE_OBSCURER=1

# XSECURELOCK_SHOW_DATETIME specifies whether to show local date and time on the login.
# 
# TODO: not yet implemented
export XSECURELOCK_SHOW_DATETIME=0

# XSECURELOCK_DATETIME_FORMAT specifies the date format to show. (see strftime(3))
# 
# TODO: not yet implemented
export XSECURELOCK_DATETIME_FORMAT=""

# XSECURELOCK_SHOW_HOSTNAME specifies whether to show the hostname on the login screen.
# 
# Possible values are 0 for not showing the hostname, 1 for showing the short form, and 2 for showing the long form.
export XSECURELOCK_SHOW_HOSTNAME=0

# XSECURELOCK_SHOW_USERNAME specifies whether to show the username on the login screen.
export XSECURELOCK_SHOW_USERNAME=0

# XSECURELOCK_DISCARD_FIRST_KEYPRESS specifies whether the first key press is part of the password.
export XSECURELOCK_DISCARD_FIRST_KEYPRESS=1

# XSECURELOCK_FONT specifies a X11 or FontConfig font name to use for auth_x11. You can get a list of supported font names by running xlsfonts and fc-list.
# 
# TODO: not yet implemented
export XSECURELOCK_FONT=""

# XSECURELOCK_FORCE_GRAB must be one of 0, 1, or 2.
# 
# for more information see https://github.com/google/xsecurelock#forcing-grabs
export XSECURELOCK_FORCE_GRAB=0

# XSECURELOCK_SAVER specifies the the file name in $libexecdir/xsecurelock of the desired screen saver.
# 
# Names should start with saver_
export XSECURELOCK_SAVER="saver_blank"

# XSECURELOCK_GLOBAL_SAVER specifies the file name in $libexecdir/xsecurelock of the desired global screen saver module (by default this is a multiplexer that runs XSECURELOCK_SAVER on each screen).
# 
# Names should start with saver_
export XSECURELOCK_GLOBAL_SAVER="saver_multiplex"

# XSECURELOCK_SAVER_RESET_ON_AUTH_CLOSE specifies whether to signal the saver module with SIGUSR1 when the auth dialog closes.
export XSECURELOCK_SAVER_RESET_ON_AUTH_CLOSE=0

# XSECURELOCK_SWITCH_USER_COMMAND is a shell command to execute when Win-O or Ctrl-Alt-O are pressed (think "other user").
export XSECURELOCK_SWITCH_USER_COMMAND=""

# XSECURELOCK_XSCREENSAVER_PATH specifies the location where XScreenSaver hacks are installed for use by saver_xscreensaver.
export XSECURELOCK_XSCREENSAVER_PATH=""

# XSECURELOCK_KEY_%S_COMMAND specifies a shell command to execute when the specified key is pressed.
# 
# The key is set by replacing %s with a X11 keysym (find using xev)
# 
# For example: XSECURELOCK_KEY_XF86AudioPlay_COMMAND='playerctl play-pause'
#export XSECURELOCK_KEY_%S_COMMAND=""

# XSECURELOCK_NO_COMPOSITE specifies whether to disable covering the composite overlay window and switch to a more traditional way of locking, but may allow desktop notifications to be visible on top of the screen lock.
# 
# NOTE: Not recommended
export XSECURELOCK_NO_COMPOSITE=0

# XSECURELOCK_NO_PAM_RHOST tells authproto_pam not to set PAM_RHOST to localhost, despite recommendation to do so by the Linux-PAM Application Developers' Guide.
# 
# This may work around bugs in third-party PAM authentication modules. If this solves a problem for you, please report a bug against said PAM module.
export XSECURELOCK_NO_PAM_RHOST=0

# XSECURELOCK_PAM_SERVICE specifies the PAM service name.
# 
# You should have a file with that name in /etc/pam.d.
export XSECURELOCK_PAM_SERVICE="system-auth"

exec auth_xringlock
