/*
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <stdbool.h>
#include <cairo.h>

struct backend;
typedef struct backend backend_t;

backend_t *backend_new();

void backend_destroy(backend_t *b);

cairo_surface_t *backend_get_surface(backend_t *b, int *out_width, int *out_height);

void backend_flush(backend_t *b);

void backend_set_next_blocks(backend_t *b);

void backend_unset_next_blocks(backend_t *b);

char backend_next_key(backend_t *b, char *ch, struct timespec *timeout);

bool backend_caps(backend_t *b);
