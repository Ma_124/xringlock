/*
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <cairo/cairo.h>

struct color {
    unsigned char flags;
    double r;
    double g;
    double b;
    double a;
};

typedef struct color color_t;

#define TRANSPARENT_COLOR &transparent_color
static color_t transparent_color;

color_t *new_color();
color_t *new_randomized_color();
void cairo_set_source_color(cairo_t *cr, color_t *color);
void cairo_set_source_color_with_alpha(cairo_t *cr, color_t *color, double alpha);

cairo_surface_t *load_image(const char* filepath, int width, int height);
