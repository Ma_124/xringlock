/*
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <cairo.h>
#include "state.h"

#define INDICATOR_RING    ((unsigned char) 0)

#define INDICATOR_DEFAULT INDICATOR_RING
#define INDICATOR_DEFAULT_NAME "ring"

#define EVENT_TYPE_RENDER 0
#define EVENT_TYPE_INIT   1
#define EVENT_TYPE_KEY    2
#define EVENT_TYPE_BACK   3
#define EVENT_TYPE_CLEAR  4
#define EVENT_TYPE_PAM    5

typedef unsigned char indicator_t;
typedef void *indicator_state_t;

indicator_state_t *indicator_new(char *name, indicator_t *out_indicator);
void indicator_destroy(indicator_t indicator, indicator_state_t *in);

void indicator_draw(indicator_t indicator, indicator_state_t *in,
                    state_t *st, cairo_t *cr, unsigned long time, char event_type);

bool indicator_is_animating(indicator_t indicator, indicator_state_t *in, unsigned long time);

#define INDICATOR(name) indicator_state_t *indicator_##name##_new(); \
                        void indicator_##name##_destroy(indicator_state_t *in); \
                        void indicator_##name##_draw(indicator_state_t *in, \
                                        state_t *st, cairo_t *cr, unsigned long time, char event_type); \
                        bool indicator_##name##_is_animating(indicator_state_t *in, unsigned long time)
