/*
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <malloc.h>
#include <fcntl.h>
#include <unistd.h>
#include <cairo-xlib.h>
#include <stdlib.h>
#include <X11/extensions/Xrandr.h>

#ifdef HAVE_XKB
#include <X11/XKBlib.h>
#endif

#include "backend.h"
#include "logging.h"
#include "config.h"

#define FD_KEYS 0

struct backend {
    Display *d;
    bool blocking;
    fd_set fd_keys_set;
};

backend_t *backend_new() {
    backend_t *b = malloc(sizeof(*b));
    b->blocking = true;
    FD_ZERO(&b->fd_keys_set);
    FD_SET(FD_KEYS, &b->fd_keys_set); // NOLINT(hicpp-signed-bitwise)
    return b;
}

void backend_destroy(backend_t *b) {
    XCloseDisplay(b->d);
    free(b);
}

static void get_windows(Display *d, Window *main, Window *parent, Window *root);

cairo_surface_t *backend_get_surface(backend_t *b, int *out_width, int *out_height) {
    // open display
    b->d = XOpenDisplay(NULL);
    if (b->d == NULL) {
        Log("could not open display");
        return NULL;
    }

    // get {main,root,parent}_win
    Window main_win, parent_win, root_win;
    get_windows(b->d, &main_win, &parent_win, &root_win);

    // == get monitor dimensions ==
    int num_monitors = 0;
    XRRMonitorInfo *monitors = XRRGetMonitors(b->d, root_win, True, &num_monitors);
    if (num_monitors == 0 || monitors == NULL) {
        Log("cannot get monitor info");
        monitors = NULL;
        return NULL;
    }

    long main_monitor = CONFIG_PREFERRED_MONITOR;
    if (main_monitor < 0) {
        Log("MONITOR: expected positive value, got %ld. using default 0.", main_monitor);
        main_monitor = 0;
    } else if (main_monitor > num_monitors) {
        Log("MONITOR: requested monitor %ld only plugged in 0-%d. using default 0.", main_monitor,
            num_monitors);
        main_monitor = 0;
    }

    *out_width = monitors[main_monitor].width;
    *out_height = monitors[main_monitor].height;

    // get VisualInfo
    int screen = DefaultScreen(b->d);
    XVisualInfo vinfo;
    if (XMatchVisualInfo(b->d, DefaultScreen(b->d), 32, TrueColor, &vinfo) == 0) {
        Log("no matching visual was found");
        vinfo.visual = DefaultVisual(b->d, screen);
    }

    // create transparent attrs
    XSetWindowAttributes attr;
    attr.colormap = XCreateColormap(b->d, DefaultRootWindow(b->d), vinfo.visual, AllocNone);
    attr.border_pixel = 0;
    attr.background_pixel = 0;

#pragma clang diagnostic push
#pragma ide diagnostic ignored "hicpp-signed-bitwise"
    // create window
    Window window = XCreateWindow(
            b->d,                     // display
            parent_win,               // parent
            monitors[main_monitor].x, // y
            monitors[main_monitor].y, // y
            *out_width, *out_height,  // dimensions
            0,                        // border_width
            vinfo.depth,              // depth
            InputOutput,              // class
            vinfo.visual,             // visual
            CWColormap |              // (valuemask)
            CWBorderPixel |           //
            CWBackPixel,              //
            &attr);                   // attributes

    XSelectInput(b->d, window, ButtonPressMask | KeyPressMask | StructureNotifyMask);
#pragma clang diagnostic pop

    // Show window
    XMapWindow(b->d, window);

    // free monitors
    XFree(monitors);

    // create cairo surface
    cairo_surface_t *surface = cairo_xlib_surface_create(b->d, window, vinfo.visual, *out_width, *out_height);
    cairo_xlib_surface_set_size(surface, *out_width, *out_height);

    return surface;
}

//! get_windows reads the main, parent, and root window from the environment
static void get_windows(Display *d, Window *main, Window *parent, Window *root) {
    // get {root,parent}_win
    *main = CONFIG_XSCREENSAVER_WINDOW;
    if (*main == None) {
        Log("no XSCREENSAVER_WINDOW supplied");
        return;
    }

    // we don't really care about children
    Window *_children = NULL;
    unsigned int _num_children;
    int status = XQueryTree(d, *main, root, parent, &_children, &_num_children);
    if (status == 0) {
        Log("could not detect parent_win. XQueryTree failed.");
        *main = None;
        return;
    }
    XFree(_children);
}

void backend_flush(backend_t *b) {
    XEvent ev;

    while (XPending(b->d))
        XNextEvent(b->d, &ev);
}

void backend_set_next_blocks(backend_t *b) {
    int flags = fcntl(FD_KEYS, F_GETFL, 0);
    fcntl(FD_KEYS, F_SETFL, (~flags) & O_NONBLOCK); // NOLINT(hicpp-signed-bitwise)
    b->blocking = true;
}

void backend_unset_next_blocks(backend_t *b) {
    int flags = fcntl(FD_KEYS, F_GETFL, 0);
    fcntl(FD_KEYS, F_SETFL, flags | O_NONBLOCK); // NOLINT(hicpp-signed-bitwise)
    b->blocking = false;
}

char backend_next_key(backend_t *b, char *ch, struct timespec *timeout) {
    if (b->blocking && (timeout->tv_sec > 0 || timeout->tv_nsec > 0)) {
        int numberOfFDs = pselect(1, &b->fd_keys_set, NULL, NULL, timeout, NULL);
        if (numberOfFDs < 0) {
            LogErrno("pselect");
            return false;
        }
        if (numberOfFDs == 0) {
            return 126;
        }
    }

    ssize_t l = read(FD_KEYS, ch, 1);
    if (l == 1)
        return true;
    if (l == 0) {
        Log("reached EOF");
        return 127;
    }
    if (l < 0) {
        if (errno != EWOULDBLOCK)
            LogErrno();
        else
            *ch = 0;
    }
    if (l > 0) {
        // should have been impossible
        Log("read more than one key");
        abort();
    }
    return false;
}

bool backend_caps(backend_t *b) {
    unsigned int n;
    XkbGetIndicatorState(b->d, XkbUseCoreKbd, &n);
    return n & (unsigned int) 1;
}
