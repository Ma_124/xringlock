/*
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <malloc.h>

#include "ring_indicator.h"
#include "config.h"
#include "logging.h"
#include "easings.h"

static double max1(double x);

struct indicator_ring_state {
    color_t *last_fill_color;
    color_t *last_border_color;
    unsigned long last_event_time;
    double last_angle;
};

typedef struct indicator_ring_state indicator_ring_state_t;

void indicator_ring_draw_only_ring(state_t *st, cairo_t *cr, indicator_ring_state_t *inst, double w, double h, bool initial, double alpha);

indicator_state_t *indicator_ring_new() {
    indicator_ring_state_t *in = malloc(sizeof(*in));
    in->last_fill_color = CONFIG_RING_FILL_COLOR;
    in->last_border_color = CONFIG_RING_BORDER_COLOR;
    in->last_event_time = 0;
    return (indicator_state_t *) in;
}

void indicator_ring_destroy(indicator_state_t *in) {
    indicator_ring_state_t *inst = (indicator_ring_state_t *) in;
    free(inst);
}

void indicator_ring_draw(indicator_state_t *in, state_t *st, cairo_t *cr, unsigned long now, char event_type) {
    Trace("");
    indicator_ring_state_t *inst = (indicator_ring_state_t *) in;

    double w = state_get_width(st);
    double h = state_get_height(st);

    // TODO set last_event_time when event_type goes away from PAM (solid blue during auth then fade if wrong)
    double alpha;
    // get color based on event_type
    switch (event_type) {
        case EVENT_TYPE_INIT:
            inst->last_fill_color = TRANSPARENT_COLOR;
            inst->last_border_color = TRANSPARENT_COLOR;
            Trace("event type init");
            goto normal_color;
        case EVENT_TYPE_KEY:
            inst->last_fill_color = CONFIG_RING_FILL_COLOR_KEY;
            inst->last_border_color = CONFIG_RING_BORDER_COLOR_KEY;
            Trace("event type key");
            goto normal_color;
        case EVENT_TYPE_BACK:
            inst->last_fill_color = CONFIG_RING_FILL_COLOR_BACK;
            inst->last_border_color = CONFIG_RING_BORDER_COLOR_BACK;
            Trace("event type back");
            goto normal_color;
        case EVENT_TYPE_CLEAR:
            inst->last_fill_color = CONFIG_RING_FILL_COLOR_CLEAR;
            inst->last_border_color = CONFIG_RING_BORDER_COLOR_CLEAR;
            Trace("event type clear");
            goto normal_color;
        case EVENT_TYPE_PAM:
            inst->last_fill_color = CONFIG_RING_FILL_COLOR_AUTH;
            inst->last_border_color = CONFIG_RING_BORDER_COLOR_AUTH;
            Trace("event type auth");
            goto normal_color;
        default:
            Log("unknown event type %d", event_type);
        case EVENT_TYPE_RENDER:
            alpha = ease(CONFIG_KEY_EASING_CURVE, 1 - max1((double) (now - inst->last_event_time) / CONFIG_KEY_EASING_DURATION));

            indicator_ring_draw_only_ring(st, cr, inst, w, h, false, alpha);

            Trace("event type render");
            goto all_colors;
    }
    normal_color:
    Trace("normal_color");

    inst->last_event_time = now;

    indicator_ring_draw_only_ring(st, cr, inst, w, h, true, 1);

    all_colors:
    Trace("all_colors");

    if (event_type != EVENT_TYPE_RENDER)
        inst->last_event_time = now;
}

void indicator_ring_draw_only_ring(state_t *st, cairo_t *cr, indicator_ring_state_t *inst, double w, double h, bool initial, double alpha) {
    double mw = w / 2.0;
    double mh = h / 2.0;

    // arc full surface
    cairo_arc(cr, mw, mh, CONFIG_RING_RADIUS, 0, 2 * M_PI);

    // fill with base color
    cairo_set_source_color(cr, CONFIG_RING_FILL_COLOR);
    cairo_fill_preserve(cr);

    // only cairo_set_source_color recalculates a random color
    if (initial)
        cairo_set_source_color(cr, inst->last_fill_color);
    else
        cairo_set_source_color_with_alpha(cr, inst->last_fill_color, alpha);

    if (state_has_caps(st)) {
        cairo_fill_preserve(cr);

        cairo_set_source_color(cr, CONFIG_RING_FILL_COLOR_CAPS);
    }

    // fill
    cairo_fill(cr);

    // arc ring, offset by width/2 so that the fill and stroke don't overlap
    cairo_arc(cr, mw, mh, CONFIG_RING_RADIUS+(CONFIG_RING_BORDER_WIDTH/2), 0, CONFIG_KEY_RING_ANGLE);
    cairo_set_line_width(cr, CONFIG_RING_BORDER_WIDTH);

    cairo_set_source_color(cr, CONFIG_RING_BORDER_COLOR);

    if (CONFIG_KEY_RING_ANGLE != 2*M_PI) {
        // stroke whole ring and discard path
        cairo_stroke(cr);

        // get new angle offset
        if (initial)
            inst->last_angle = drand48()*2*M_PI;

        // new sub path
        cairo_arc(cr, mw, mh, CONFIG_RING_RADIUS+(CONFIG_RING_BORDER_WIDTH/2), inst->last_angle, inst->last_angle+CONFIG_KEY_RING_ANGLE);
    } else {
        // stroke whole ring and preserve path
        cairo_stroke_preserve(cr);
    }

    if (initial)
        cairo_set_source_color(cr, inst->last_border_color);
    else
        cairo_set_source_color_with_alpha(cr, inst->last_border_color, alpha);

    if (state_has_caps(st)) {
        cairo_stroke_preserve(cr);

        cairo_set_source_color(cr, CONFIG_RING_BORDER_COLOR_CAPS);
    }

    cairo_stroke(cr);
}

bool indicator_ring_is_animating(indicator_state_t *in, unsigned long time) {
    indicator_ring_state_t *inst = (indicator_ring_state_t *) in;
    return (time - inst->last_event_time) <= (unsigned long) (CONFIG_KEY_EASING_DURATION);
}

static double max1(double x) {
    if (x <= 0)
        return 0;
    if (x >= 1)
        return 1;
    return x;
}
