/*
 * Copyright 2021 Ma_124 <ma_124+oss@pm.me>. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <string.h>

#include "indicator.h"
#include "ring_indicator.h"
#include "logging.h"
#include "util.h"

indicator_state_t *indicator_new(char *name, indicator_t *out_indicator) {
    static indicator_state_t *(*const indicator_new_fns[])() = {indicator_ring_new};

    if (strcmp(name, "ring") == 0) {
        *out_indicator = INDICATOR_RING;
    } else {
        Log("unknown indicator %s, defaulting to " INDICATOR_DEFAULT_NAME, name);
        *out_indicator = INDICATOR_DEFAULT;
    }

    if (*out_indicator >= ARRAY_LEN(indicator_new_fns)) {
        Log("indicator_new: index %d outside of function table", *out_indicator);
        abort();
    }

    return indicator_new_fns[*out_indicator]();
}

void indicator_destroy(indicator_t indicator, indicator_state_t *in) {
    static void (*const indicator_destroy_fns[])(indicator_state_t *in) = {indicator_ring_destroy};

    if (indicator >= ARRAY_LEN(indicator_destroy_fns)) {
        Log("indicator_destroy: index %d outside of function table", indicator);
        abort();
    }

    indicator_destroy_fns[indicator](in);
}

void indicator_draw(indicator_t indicator, indicator_state_t *in, state_t *st, cairo_t *cr, unsigned long time,
                    char event_type) {
    static void (*const indicator_draw_fns[]) (indicator_state_t *in, state_t *st,
            cairo_t *cr, unsigned long time, char event_type) = {indicator_ring_draw};

    if (indicator >= ARRAY_LEN(indicator_draw_fns)) {
        Log("indicator_draw: index %d outside of function table", indicator);
        abort();
    }

    indicator_draw_fns[indicator](in, st, cr, time, event_type);
    Trace("finished drawing indicator");
}

bool indicator_is_animating(indicator_t indicator, indicator_state_t *in, unsigned long time) {
    static bool (*const indicator_is_animating_fns[]) (indicator_state_t *in, unsigned long time) = {indicator_ring_is_animating};

    if (indicator >= ARRAY_LEN(indicator_is_animating_fns)) {
        Log("indicator_is_animating_fns: index %d outside of function table", indicator);
        abort();
    }

    return indicator_is_animating_fns[indicator](in, time);
}
